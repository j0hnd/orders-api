<?php

namespace App\Repository;

use App\Entity\Order;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends ServiceEntityRepository
{
	private $manager;


	public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
	{
		parent::__construct($registry, Order::class);
		$this->manager = $manager;
	}

	public function save($data)
	{
		if (empty($data)) {
			return null;
		}

		if (! isset($data['total_amount']) and ! isset($data['discount_amount'])) {
			return null;
		}

		if (empty($data['total_amount']) and $data['discount_amount']) {
			return null;
		}

		$order = new Order();
		$order->setTotalAmount($data['total_amount'])
			->setDiscountAmount($data['discount_amount'])
			->setStatus(Order::ORDER_RECEIVED)
			->setAddedBy(1)
			->setCreatedAt(new \DateTime('now'))
			->setUpdatedAt(new \DateTime('now'));

		$this->manager->persist($order);
		$this->manager->flush();

		return $order;
	}

	public function update(Order $order): Order
	{
		$this->manager->persist($order);
		$this->manager->flush();

		return $order;
	}

	public function findByNot($field, $value)
	{
		$qb = $this->createQueryBuilder('a');
		$qb->where($qb->expr()->not($qb->expr()->eq('a.'.$field, '?1')));
		$qb->setParameter(1, $value);

		return $qb->getQuery()
			->getResult();
	}

	// /**
	//  * @return Order[] Returns an array of Order objects
	//  */
	/*
	public function findByExampleField($value)
	{
		return $this->createQueryBuilder('o')
			->andWhere('o.exampleField = :val')
			->setParameter('val', $value)
			->orderBy('o.id', 'ASC')
			->setMaxResults(10)
			->getQuery()
			->getResult()
		;
	}
	*/

	/*
	public function findOneBySomeField($value): ?Order
	{
		return $this->createQueryBuilder('o')
			->andWhere('o.exampleField = :val')
			->setParameter('val', $value)
			->getQuery()
			->getOneOrNullResult()
		;
	}
	*/
}
