<?php

namespace App\Repository;

use App\Entity\OrderItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OrderItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrderItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrderItem[]    findAll()
 * @method OrderItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderItemRepository extends ServiceEntityRepository
{
	private $manager;


	public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
	{
		parent::__construct($registry, OrderItem::class);
		$this->manager = $manager;
	}

	public function save($data)
	{
		if (empty($data)) {
			return null;
		}

		if (! isset($data['order_id']) and ! isset($data['item_name'])) {
			return null;
		}

		if (empty($data['order_id']) and empty($data['item_name'])) {
			return null;
		}

		$order_item = new OrderItem();
		$order_item->setOrderId($data['order_id'])
			->setItemName($data['item_name'])
			->setCreatedAt(new \DateTime('now'))
			->setUpdatedAt(new \DateTime('now'));

		$this->manager->persist($order_item);
		$this->manager->flush();

		return $order_item;
	}

	// /**
	//  * @return OrderItem[] Returns an array of OrderItem objects
	//  */
	/*
	public function findByExampleField($value)
	{
		return $this->createQueryBuilder('o')
			->andWhere('o.exampleField = :val')
			->setParameter('val', $value)
			->orderBy('o.id', 'ASC')
			->setMaxResults(10)
			->getQuery()
			->getResult()
		;
	}
	*/

	/*
	public function findOneBySomeField($value): ?OrderItem
	{
		return $this->createQueryBuilder('o')
			->andWhere('o.exampleField = :val')
			->setParameter('val', $value)
			->getQuery()
			->getOneOrNullResult()
		;
	}
	*/
}
