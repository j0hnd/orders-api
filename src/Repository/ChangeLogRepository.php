<?php

namespace App\Repository;

use App\Entity\ChangeLog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ChangeLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method ChangeLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method ChangeLog[]    findAll()
 * @method ChangeLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChangeLogRepository extends ServiceEntityRepository
{
    private $manager;


    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, ChangeLog::class);
        $this->manager = $manager;
    }

    public function createLog($data)
    {
        if (empty($data)) {
            return null;
        }

        if (! isset($data['order_id']) and ! isset($data['action'])) {
            return null;
        }

        if (empty($data['order_id']) and empty($data['action'])) {
            return null;
        }

        switch ($data['action']) {
            case "order_processing";
                $description = "Order #<order_no> has been changed to PROCESSING";
                $description = str_replace('<order_no>', $data['order_id'], $description);
                break;

            case "order_ready_to_ship";
                $description = 'Order #<order_no> has been changed to READY TO SHIP';

                if ($data['reason_status'] and $data['reason_details']) {
                    $description .= ' with reason: <reason_status> (<reason_details>)';
                }

                $description = str_replace('<order_no>', $data['order_id'], $description);

                break;

            case "order_shipped";
                $description = 'Order #<order_no> has been changed to SHIPPED';
                $description = str_replace('<order_no>', $data['order_id'], $description);
                break;

            case "order_cancelled";
                $description = 'Order #<order_no> has been CANCELLED';
                $description = str_replace('<order_no>', $data['order_id'], $description);
                break;

            default:
                $description = "Order #<order_no> has been received by the system";
                $description = str_replace('<order_no>', $data['order_id'], $description);
                break;
        }

        $change_log = new ChangeLog();

        $change_log
            ->setOrderId($data['order_id'])
            ->setAction($data['action'])
            ->setDescription($description)
            ->setCreatedAt(new \DateTime('now'));


        $this->manager->persist($change_log);
        $this->manager->flush();

        return $change_log->getId();
    }

    // /**
    //  * @return ChangeLog[] Returns an array of OrderItem objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OrderItem
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
