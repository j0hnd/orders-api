<?php

namespace App\Repository;

use App\Entity\ShippingDetail;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Uid\Uuid;

/**
 * @method ShippingDetail|null find($id, $lockMode = null, $lockVersion = null)
 * @method ShippingDetail|null findOneBy(array $criteria, array $orderBy = null)
 * @method ShippingDetail[]    findAll()
 * @method ShippingDetail[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShippingDetailRepository extends ServiceEntityRepository
{
	const SHIPPING_PENDING = "pending";
	const SHIPPING_PROCCESSED = "processed";

	private $manager;

	public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
	{
		parent::__construct($registry, ShippingDetail::class);
		$this->manager = $manager;
	}

	public function save()
	{
		$shipping = new ShippingDetail();
		$shipping
            ->setShippingCompany('-')
			->setStatus(self::SHIPPING_PENDING)
			->setCreatedAt(new \DateTime('now'))
			->setUpdatedAt(new \DateTime('now'));

		$this->manager->persist($shipping);
		$this->manager->flush();

		return $shipping;
	}

	// /**
	//  * @return ShippingDetail[] Returns an array of ShippingDetail objects
	//  */
	/*
	public function findByExampleField($value)
	{
		return $this->createQueryBuilder('s')
			->andWhere('s.exampleField = :val')
			->setParameter('val', $value)
			->orderBy('s.id', 'ASC')
			->setMaxResults(10)
			->getQuery()
			->getResult()
		;
	}
	*/

	/*
	public function findOneBySomeField($value): ?ShippingDetail
	{
		return $this->createQueryBuilder('s')
			->andWhere('s.exampleField = :val')
			->setParameter('val', $value)
			->getQuery()
			->getOneOrNullResult()
		;
	}
	*/
}
