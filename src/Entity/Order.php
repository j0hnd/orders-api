<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Order
 *
 * @ORM\Table(name="orders", uniqueConstraints={@ORM\UniqueConstraint(name="UNIQ_F5299398D8177B3F", columns={"box_id"})}, indexes={@ORM\Index(name="added_by_idx", columns={"added_by"}), @ORM\Index(name="shipping_id_idx", columns={"shipping_id"}), @ORM\Index(name="status_idx", columns={"status"})})
 * @ORM\Entity(repositoryClass="App\Repository\OrderRepository")
 */
class Order
{
	const ORDER_RECEIVED = "order_received";
	const ORDER_CANCELED = "order_cancelled";
	const ORDER_PROCESSING = "order_processing";
	const ORDER_READY_TO_SHIP = "order_ready_to_ship";
	const ORDER_SHIPPED = "order_shipped";

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="box_id", type="integer", nullable=true)
     */
    private $boxId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="shipping_id", type="integer", nullable=true)
     */
    private $shippingId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="status", type="string", length=20, nullable=true)
     */
    private $status;

    /**
     * @var float|null
     *
     * @ORM\Column(name="total_amount", type="float", precision=10, scale=0, nullable=true)
     */
    private $totalAmount;

    /**
     * @var float|null
     *
     * @ORM\Column(name="discount_amount", type="float", precision=10, scale=0, nullable=true)
     */
    private $discountAmount;

    /**
     * @var int
     *
     * @ORM\Column(name="added_by", type="integer", nullable=false)
     */
    private $addedBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

	/**
	 * @ORM\Column(type="integer", nullable=true)
	 */
	private $cancelled_by;

	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	private $cancelled_at;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBoxId(): ?int
    {
        return $this->boxId;
    }

    public function setBoxId(?int $boxId): self
    {
        $this->boxId = $boxId;

        return $this;
    }

    public function getShippingId(): ?int
    {
        return $this->shippingId;
    }

    public function setShippingId(?int $shippingId): self
    {
        $this->shippingId = $shippingId;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getTotalAmount(): ?float
    {
        return $this->totalAmount;
    }

    public function setTotalAmount(?float $totalAmount): self
    {
        $this->totalAmount = $totalAmount;

        return $this;
    }

    public function getDiscountAmount(): ?float
    {
        return $this->discountAmount;
    }

    public function setDiscountAmount(?float $discountAmount): self
    {
        $this->discountAmount = $discountAmount;

        return $this;
    }

    public function getAddedBy(): ?int
    {
        return $this->addedBy;
    }

    public function setAddedBy(int $addedBy): self
    {
        $this->addedBy = $addedBy;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

	public function getCancelledBy(): ?int
	{
		return $this->cancelled_by;
	}

	public function setCancelledBy(?int $cancelled_by): self
	{
		$this->cancelled_by = $cancelled_by;

		return $this;
	}

	public function getCancelledAt(): ?\DateTimeInterface
	{
		return $this->cancelled_at;
	}

	public function setCancelledAt(?\DateTimeInterface $cancelled_at): self
	{
		$this->cancelled_at = $cancelled_at;

		return $this;
	}
}
