<?php

namespace App\Controller;


use App\Repository\ChangeLogRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use \Doctrine\Common\Collections\Criteria;
use App\Entity\Order;
use App\Repository\OrderItemRepository;
use App\Repository\OrderRepository;
use App\Repository\ShippingDetailRepository;


class OrderController extends AbstractController
{
	private $orderRepository;

	private $orderItemRepository;

	private $shippingDetailRepository;

	private $changeLogRepository;

	private $validator;


	public function __construct(OrderRepository $orderRepository,
		OrderItemRepository $orderItemRepository,
		ShippingDetailRepository $shippingDetailRepository,
		ChangeLogRepository $changeLogRepository,
		ValidatorInterface $validator)
	{
		$this->orderRepository = $orderRepository;
		$this->orderItemRepository = $orderItemRepository;
		$this->shippingDetailRepository = $shippingDetailRepository;
		$this->changeLogRepository = $changeLogRepository;
		$this->validator = $validator;
	}

    /**
	 * @param Request $request
	 * @return JsonResponse
	 * @throws Exception
     * @Route("/api/order", name="store", methods={"POST"})
     */
    public function store(Request $request): JsonResponse
    {
    	try {
			$data = json_decode($request->getContent(), true);

			$rules = new Assert\Collection([
				'total_amount' => [
					new Assert\NotBlank(),
					new Assert\PositiveOrZero(),
				],
				'discount_amount' => new Assert\NotBlank(),
				'item_name' => new Assert\NotBlank()
			]);

			$validation_result = $this->validator->validate($data, $rules);

			if (0 === count($validation_result)) {
				$order = $this->orderRepository->save($data);

				if ($order) {
					$order_item_data = [
						'order_id' => $order->getId(),
						'item_name' => $data['item_name']
					];

					if ($this->orderItemRepository->save($order_item_data)) {
						if ($shipping_details = $this->shippingDetailRepository->save()) {
							$order->setShippingId($shipping_details->getId());

							if ($this->orderRepository->update($order)) {

                                $this->changeLogRepository->createLog(['order_id' => $order->getId(), 'action' => Order::ORDER_RECEIVED]);

								return new JsonResponse([
									'success' => true,
									'message' => 'Order is created!',
									'data' => [
										'order_id' => $order->getId()
									]
								], Response::HTTP_CREATED);
							}
						} else {
							$message = "Unable to create shipping details";
						}
					} else {
						$message = "Unable to create order item(s)";
					}
				} else {
					$message = "Unable to save the order";
				}
			} else {
				$field = str_replace('"', "", $validation_result[0]->getParameters()["{{ field }}"]);
				$message = str_replace("field", $field, $validation_result[0]->getMessageTemplate());
			}
		} catch (\Exception $exception) {
			return new JsonResponse([
				'success' => false,
				'code' => $exception->getCode(),
				'errors' => $exception->getMessage()
			], Response::HTTP_BAD_REQUEST);
		}

		return new JsonResponse([
			'success' => false,
			'message' => $message
		], Response::HTTP_BAD_REQUEST);
    }

	/**
	 * @param Request $request
	 * @return JsonResponse
	 * @throws Exception
	 * @Route("/api/order/cancel/{order_id}/{cancelled_by}", name="cancel_order", methods={"PUT"})
	 */
    public function cancel_order(Request $request): JsonResponse
	{
		try {
			$data = ['order_id' => $request->get('order_id'), 'cancelled_by' => $request->get('cancelled_by')];

			$success = false;

			$rules = new Assert\Collection([
				'order_id' => [
					new Assert\NotBlank(),
					new Assert\Positive()
				],
				'cancelled_by' => [
					new Assert\NotBlank(),
					new Assert\Positive()
				]
			]);

			$validation_result = $this->validator->validate($data, $rules);

			if (0 === count($validation_result)) {
				$criteria = new Criteria();
				$criteria
					->where(Criteria::expr()->neq('status', Order::ORDER_CANCELED))
					->andWhere(Criteria::expr()->eq('id', $data['order_id']));

				$order = $this->orderRepository->matching($criteria)->first();

				if ($order) {
					$order
						->setStatus(Order::ORDER_CANCELED)
						->setUpdatedAt(new \DateTime('now'))
						->setCancelledBy($data['cancelled_by'])
						->setCancelledAt(new \DateTime('now'));

					if ($this->orderRepository->update($order)) {
                        $this->changeLogRepository->createLog(['order_id' => $order->getId(), 'action' => Order::ORDER_CANCELED]);

						return new JsonResponse([
							'success' => true,
							'message' => 'Order has been cancelled'
						], Response::HTTP_OK);
					}
				} else {
					$message = "Order ID not valid or the order is already cancelled";
				}
			} else {
				$field = str_replace('"', "", $validation_result[0]->getParameters()["{{ field }}"]);
				$message = str_replace("field", $field, $validation_result[0]->getMessageTemplate());
			}
		} catch (\Exception $exception) {
			return new JsonResponse([
				'success' => false,
				'code' => $exception->getCode(),
				'errors' => $exception->getMessage()
			], Response::HTTP_BAD_REQUEST);
		}

		return new JsonResponse([
			'success' => $success,
			'message' => $message
		], Response::HTTP_BAD_REQUEST);
	}
}
